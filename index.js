require('dotenv').config();

const express = require('express');

const app = express();

const endpoints = require('./endpoints');

app.use(express.json());

app.use('/', endpoints);

app.listen(process.env.PORT)

module.exports = app;