const { expect: chai_expect } = require('chai');

const supertest = require('supertest');

const app = require('../../index');

let user_token = '';

const { demo_users, demo_accounts } = require('../../db/data_samples');

const [first_user, second_user] = demo_users;

const [first_account, { balance: destination_number }] = demo_accounts;

describe('Check user end-points', checkStoreRoutes);

function checkStoreRoutes() {
  this.timeout(0);

  it('try user login', tryUserLogin);

  it('try topup balance', tryTopupBalance);

  it('try transfer balance', tryTransferBalance);

  it('try user logout', tryUserLogout);
}

async function tryUserLogin() {
  const { username, password } = first_user;

  let { body: { token } } = await supertest(app)
    .post('/user/login')
    .send({ username, password })
    .expect(200);

  user_token = token;
};

async function tryTopupBalance () {
  const amount = 5000;

  let { body } = await supertest(app)
    .post('/user/topup')
    .set('Authorization', user_token)
    .send({ amount })
    .expect(200);
};

async function tryTransferBalance () {
  const amount = 2000;

  let { body } = await supertest(app)
    .post('/user/transfer')
    .set('Authorization', user_token)
    .send({ amount, destination_number })
    .expect(200);
};

async function tryUserLogout () {
  let { body } = await supertest(app)
    .post('/user/logout')
    .set('Authorization', user_token)
    .expect(200);
};