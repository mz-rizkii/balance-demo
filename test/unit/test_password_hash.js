const { expect } = require('chai');

const { hashPassword, verifyPassword } = require('../../handlers/hash_helper')

it('check hash validation', checkPasswordHash);

async function checkPasswordHash () {
  const input = 'jirolugerr';
  
  const invalid_input = 'jirolupatt';

  const hashed = await hashPassword(input);

  const result = await verifyPassword(input, hashed);

  const invalid_result = await verifyPassword(invalid_input, hashed);

  expect(result, 'validated password should be true').to.equal(true);

  expect(invalid_result, 'invalid password should be true').to.equal(false);
}