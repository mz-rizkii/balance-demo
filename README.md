# balance demo api service
# requirements
- nodejs
- mysql db
# dependencies
- express: http API framework
- sequelize: ORM libary for CRUD operation  
- sequelize-cli: helper library to run sequelize commands from cli
- mysql2: mysql connector library for sequelize
- bcrypt: helper to hash password
- jsonwebtoken: helper to generate & verify jwt
- dotenv: helper to get values from .env file

# installation
- on cli run `npm install`, to install dependency
- use .env.template to set env variables for db connection, ports, etc
- run migration & seeders to insert sample data
- on cli run `npm start` to start application/api service

# Migrations & Seeders
- before running migration & seeder make sure to set db connection in .env file
- on cli run `npx sequelize db:migrate` to migrate db structures
- then, run `npx sequelize db:seed:all` to populate sample records

# project structure
- /.env.template template for .env files
- /index.js: entry point to run api service
- /.sequelizerc: mapping file for sequelize directory & its config
- /auth/index.js: middleware for user authorization
- /controllers/user_controller.js: controller which proccesses request to user resources
- /db/config.js: config for sequelize library
- /db/data_samples/index.js: contains data samples for seeders & integration test 
- /db/migrations/: migration files for table structures in db
- /db/models/: ORM model definitions for every table structures in db
- /db/repositories/: helpers modules for CRUD operation to table in db 
- /db/seeders/: seeder files to generate sample data 
- /endpoints/index.js: mapping every api resources to its controllers
- /handlers/balance_helper.js: module to process business logic for user_balance
- /handlers/hash_helper.js: helper process hash password
- /handlers/jwt_helper.js: helper to generate & verify jwt token
- /handlers/response_helper.js: helper to handle api response
- /handlers/user_credential.js: module to process business logic to validate user credential
- /test/integration/test_user_routes.js : integration test to every request on user resource



