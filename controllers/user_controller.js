const router = require('express').Router();

const user_controller = require('../controllers/user_controller');

const { validateUser } = require('../auth');

const {
  sendErrorResponse,
  sendSuccessResponse
} = require('../handlers/response_helper');

const {
  validateUserLogin,
  generateUserToken
} = require('../handlers/user_credential');

const {
  isValidTransferDestination,
  processTopupBalance,
  processTransferBalance
} = require('../handlers/balance_helper');

const handleUserLogin = async (req, res) => {
  try {
    const { body: { username: username_or_email, password } } = req;

    const { id, username, email } = await validateUserLogin({ username_or_email, password });

    const token = await generateUserToken({ id, username, email });

    sendSuccessResponse(res, { token });
  } catch (error) {
    sendErrorResponse(res, error);
  }
};

const handleTopupBalance = async (req, res) => {
  try {
    const { body: { amount }, ip } = req;

    const { locals: { id: user_id } } = res;

    const userAgent = req.get('User-Agent');

    const { stored, latest_balance } = await processTopupBalance({ user_id, amount, ip, userAgent });

    sendSuccessResponse(res, { latest_balance, stored });
  } catch (error) {
    sendErrorResponse(res, error);
  }
};

const validateTransferDestination = async (req, res, next) => {
  try {
    const { body: { destination_number } } = req;

    const { locals } = res;

    const { is_valid, destinationBalanceId, receiverBalance } = await isValidTransferDestination(destination_number);

    if (!is_valid) {
      throw { status: 400, code: 'transfer_destination_not_found' };
    }

    res.locals = { ...locals, destinationBalanceId, receiverBalance };

    next();
  } catch (error) {

    sendErrorResponse(res, error);
  }
};

const handleTransferBalance = async (req, res) => {
  try {
    const { body: { amount, destination_number }, ip } = req;

    const { locals: { id, destinationBalanceId, receiverBalance } } = res;

    const userAgent = req.get('User-Agent');

    const { stored, latest_balance } = await processTransferBalance({
      id, destinationBalanceId, destination_number, receiverBalance, amount, ip, userAgent
    });

    sendSuccessResponse(res, { latest_balance, stored });
  } catch (error) {
    sendErrorResponse(res, error);
  }
};

const handleUserLogout = async (req, res) => {
  const logout = true;

  sendSuccessResponse(res, { logout });
};

router.post('/login', handleUserLogin);

router.post('/logout', validateUser, handleUserLogout);

router.post('/topup', validateUser, handleTopupBalance);

router.post('/transfer', validateUser, validateTransferDestination, handleTransferBalance);

module.exports = router;