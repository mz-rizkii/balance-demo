'use strict';

const user_balance = 'user_balance';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(user_balance, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER
      },
      balance: {
        type: Sequelize.STRING
      },
      balanceAchieve: {
        type: Sequelize.INTEGER
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(user_balance);
  }
};