'use strict';

const bank_balance_history = 'bank_balance_history';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(bank_balance_history, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      bankBalanceId: {
        type: Sequelize.INTEGER
      },
      balanceBefore: {
        type: Sequelize.INTEGER
      },
      balanceAfter: {
        type: Sequelize.INTEGER
      },
      activity: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.ENUM('debit', 'kredit')
      },
      ip: {
        type: Sequelize.STRING
      },
      location: {
        type: Sequelize.STRING
      },
      userAgent: {
        type: Sequelize.STRING
      },
      author: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.INTEGER.UNSIGNED
      },
      updated_at: Sequelize.INTEGER.UNSIGNED
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(bank_balance_history);
  }
};