'use strict';

const bank_balance = 'bank_balance';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(bank_balance, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER
      },
      balance: {
        type: Sequelize.STRING
      },
      balanceAchieve: {
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING
      },
      enable: {
        type: Sequelize.BOOLEAN
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(bank_balance);
  }
};