const addWhere = (where) => ({ where });

const getTimestamp = () => Math.floor(Date.now()/1000); 

module.exports = { addWhere, getTimestamp };