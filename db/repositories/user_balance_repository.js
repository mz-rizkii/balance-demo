const { sequelize } = require('../models');

const { user_balance, user_balance_history } = require('../models'); 

const { addWhere, getTimestamp } = require('./utility');

const getUserBalance = (userId) => user_balance.findOne(addWhere({ userId }));

const getUserBalanceByNumber = (balance) => user_balance.findOne(addWhere({ balance }));

const removeUserBalance = (userId) => user_balance.destroy(addWhere({ userId }));

const storeBalanceHistory = ({ userBalanceId, balanceBefore, balanceAfter, type, activity, ip, userAgent, location, author, transaction }) => {
  const created_at = getTimestamp();

  return user_balance_history.create({
    userBalanceId, balanceBefore, balanceAfter, type, ip, userAgent, location, activity, author, created_at
  }, { transaction });
};

const updateUserBalance = async ({ id, balanceAchieve, transaction }) => user_balance.update({ balanceAchieve }, { ...addWhere({ id }), transaction });

const initTransaction = async () => sequelize.transaction();

const initUserBalance = ({ userId, balance, balanceAchieve }) => user_balance.create({ userId, balance, balanceAchieve });

module.exports = {
  getUserBalance,
  getUserBalanceByNumber,
  initUserBalance,
  initTransaction, 
  removeUserBalance,
  storeBalanceHistory,
  updateUserBalance
};