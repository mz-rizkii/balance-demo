const {
  user,
  Sequelize: { Op }
} = require('../models');

const { addWhere } = require('./utility');

const getUserByCredential = async (username) => user.findOne(addWhere({ [Op.or]: [{ username }, { email: username }] }));

const removeUserByEmail = async (email) => user.destroy(addWhere({ email }));

const storeUser = ({ username, email, password }) => user.create({ username, email, password });

module.exports = {
  getUserByCredential,
  removeUserByEmail,
  storeUser
};