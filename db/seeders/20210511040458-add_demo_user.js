'use strict';

const {
  removeUserByEmail,
  storeUser
} = require('../repositories/user_repository');

const { hashPassword } = require('../../handlers/hash_helper'); 

const { demo_users } = require('../data_samples');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let added_users = [];

    for (const { password, ...detail } of demo_users) {
      const hashed = await hashPassword(password);

      added_users.push(storeUser({ password: hashed, ...detail }))
    }

    return Promise.all(added_users);
  },

  down: async (queryInterface, Sequelize) => {
    const deleted_users = demo_users.map(({ email }) => removeUserByEmail(email));

    return Promise.all(deleted_users);
  }
};
