'use strict';

const { initUserBalance, removeUserBalance } = require('../repositories/user_balance_repository');

const { getUserByCredential } = require('../repositories/user_repository');

const { demo_accounts } = require('../data_samples');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let new_accounts = [];

    for (const { username, balance, balanceAchieve } of demo_accounts) {
      const { id: userId } = await getUserByCredential(username);
      
      new_accounts.push(initUserBalance({ userId, balance, balanceAchieve }));
    }

    return Promise.all(new_accounts);
  },

  down: async (queryInterface, Sequelize) => {
    let removed_accounts = [];

    for (const { username, balance, balanceAchieve } of demo_accounts) {
      const { id: userId } = await getUserByCredential(username);
      
      removed_accounts.push(removeUserBalance(userId));
    }

    return Promise.all(removed_accounts);
  }
};
