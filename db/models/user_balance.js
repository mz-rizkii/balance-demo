'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_balance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  user_balance.init({
    userId: DataTypes.INTEGER,
    balance: DataTypes.STRING,
    balanceAchieve: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_balance', freezeTableName: true, timestamps: false
  });
  return user_balance;
};