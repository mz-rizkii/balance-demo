'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bank_balance_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  bank_balance_history.init({
    userBalanceId: DataTypes.INTEGER,
    balanceBefore: DataTypes.INTEGER,
    balanceAfter: DataTypes.INTEGER,
    activity: DataTypes.STRING,
    type: DataTypes.STRING,
    ip: DataTypes.STRING,
    location: DataTypes.STRING,
    userAgent: DataTypes.STRING,
    author: DataTypes.STRING,
    created_at: DataTypes.INTEGER.UNSIGNED,
    updated_at: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'bank_balance_history',
    underscored: true, freezeTableName: true, timestamps: false
  });
  return bank_balance_history;
};