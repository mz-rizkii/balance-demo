'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bank_balance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  bank_balance.init({
    userId: DataTypes.INTEGER,
    balance: DataTypes.STRING,
    balanceAchieve: DataTypes.INTEGER,
    code: DataTypes.STRING,
    enable: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'bank_balance',
    underscored: true, freezeTableName: true, timestamps: false
  });
  return bank_balance;
};