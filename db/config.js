require('dotenv').config();

const default_config = {
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'mysql',
  dialectOptions: {
    bigNumberStrings: true
  },
  seederStorage: 'sequelize',
  seederStorageTableName: 'sequelize_data'
};

module.exports = {
  development: default_config,
  test: default_config,
  production: default_config
};