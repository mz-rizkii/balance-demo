const initUserRecord = (username, email, password) => ({ username, email, password });

const initAccountNumber = (balance) => ({ balance, balanceAchieve: 0 });

const demo_users = [initUserRecord('user002', 'user002@demo.com', 'jajanan'), initUserRecord('user001', 'user001@demo.com', 'berjalan')];

const account_numbers = [initAccountNumber('000120008'), initAccountNumber('000120009')]

const demo_accounts = demo_users.map(({ username }, index) => ({ ...account_numbers[index], username }));

module.exports = {
  demo_users,
  demo_accounts
} 