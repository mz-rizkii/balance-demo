const router = require('express').Router();

const user_controller = require('../controllers/user_controller');

router.use('/user', user_controller);

module.exports = router;