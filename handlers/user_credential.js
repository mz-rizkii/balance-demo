const { getUserByCredential } = require('../db/repositories/user_repository');

const { verifyPassword } = require('./hash_helper');

const { generateToken, verifyToken } = require('./jwt_helper');

const bearer_prefix = 'Bearer ';

const validateUserLogin = async ({ username_or_email, password }) => {
  const player_record = await getUserByCredential(username_or_email);

  if (!player_record) {
    throw { status: 400, code: 'user_not_found'};
  }

  const { password: stored_password, id, username, email } = player_record;

  const is_valid = await verifyPassword(password, stored_password);

  if (!is_valid) {
    throw { status: 400, code: 'invalid_credential'};
  }

  return { id, username, email };
};

const generateUserToken = async ({ id, username, email }) => {
  const token = await generateToken({ id, username, email });

  return `${bearer_prefix}${token}`;
};

const verifyUserToken = async (token) => verifyToken(token.substring(bearer_prefix.length));

module.exports = {
  validateUserLogin,
  generateUserToken,
  verifyUserToken
};