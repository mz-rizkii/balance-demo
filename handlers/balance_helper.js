const {
  getUserBalance, getUserBalanceByNumber, initTransaction, storeBalanceHistory, updateUserBalance
} = require('../db/repositories/user_balance_repository');

const credit_transaction = 'kredit';

const debit_transaction = 'debit';

const makeTransactionResult = (stored, latest_balance) => ({ stored, latest_balance });

const isValidTransferDestination = async (balance) => {
  const { id: destinationBalanceId, balanceAchieve: receiverBalance } = await getUserBalanceByNumber(balance);

  const is_valid = destinationBalanceId > 0;

  return { is_valid, destinationBalanceId, receiverBalance };
};

const processTopupBalance = async ({ user_id, amount, ip, userAgent }) => {
  const transaction = await initTransaction();

  try {
    const { id: userBalanceId, balanceAchieve: balanceBefore, balance: author } = await getUserBalance(user_id);

    const balanceAfter = balanceBefore + amount;

    const location = '';

    const activity = `topup ${amount}`;

    await storeBalanceHistory({
      userBalanceId, activity, balanceBefore, balanceAfter, type: credit_transaction, ip, userAgent, location, author, transaction
    });

    await updateUserBalance({ id: userBalanceId, balanceAchieve: balanceAfter, transaction });

    await transaction.commit();

    return makeTransactionResult(true, balanceAfter);
  } catch (error) {
    await transaction.rollback();

    return makeTransactionResult(false);
  }
};

const processTransferBalance = async ({ id, destinationBalanceId, destination_number, receiverBalance, amount, ip, userAgent }) => {
  const transaction = await initTransaction();

  try {
    const { id: userBalanceId, balanceAchieve: balanceBefore, balance: author } = await getUserBalance(id);

    const balanceAfter = balanceBefore - amount;

    const location = '';

    const activity = `Transfer ${amount} to ${destination_number}`;

    await storeBalanceHistory({
      userBalanceId, activity, balanceBefore, balanceAfter, type: debit_transaction, ip, userAgent, location, author, transaction
    });

    await updateUserBalance({ id: userBalanceId, balanceAchieve: balanceAfter, transaction });

    const amountAddedByTransfer = receiverBalance + amount; 

    const transferReceived = `Transfer ${amount} fron ${author}`;

    await storeBalanceHistory({
      userBalanceId: destinationBalanceId, activity: transferReceived, balanceBefore: receiverBalance, balanceAfter: amountAddedByTransfer, 
      type: credit_transaction, ip, userAgent, location, author, transaction
    });

    await updateUserBalance({ id: destinationBalanceId, balanceAchieve: amountAddedByTransfer, transaction });

    await transaction.commit();

    return makeTransactionResult(true, balanceAfter);
  } catch (error) {
    await transaction.rollback();

    return makeTransactionResult(false);
  }
};

module.exports = {
  isValidTransferDestination,
  processTopupBalance,
  processTransferBalance
};