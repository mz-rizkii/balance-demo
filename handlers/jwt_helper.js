require('dotenv').config();

const util = require('util');

const { sign, verify } = require('jsonwebtoken');

const async_sign = util.promisify(sign);

const async_verify = util.promisify(verify);

const expiresIn = '2h';

const issuer = process.env.AUTH_ISSUER;

const token_secret = process.env.AUTH_SECRET;

const generateToken = async ({ id, username, email }) => async_sign({ id, username, email }, token_secret, { expiresIn, issuer });

const verifyToken = async (token) => async_verify(token, token_secret, { issuer });

module.exports = {
  generateToken,
  verifyToken
};