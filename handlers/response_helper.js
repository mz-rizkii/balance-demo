const sendErrorResponse = (res, { status = 500, ...error }) => {
  res.status(status).send(error);
};

const sendSuccessResponse = (res, response) => {
  res.status(200).send(response);
};

module.exports = { 
  sendErrorResponse,
  sendSuccessResponse
};