const util = require('util');

const bcrypt = require('bcrypt');

const async_hash = util.promisify(bcrypt.hash);

const async_compare = util.promisify(bcrypt.compare);

const hashPassword = (password) => async_hash(password, 10);

const verifyPassword = (incoming_password, stored_password ) => async_compare(incoming_password, stored_password);

module.exports = { hashPassword, verifyPassword };