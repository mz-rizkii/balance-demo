const { verifyUserToken } = require('../handlers/user_credential');

const validateUser = async (req, res, next) => {
  try {
    const token = req.get('Authorization');

    const { id, username, email } = await verifyUserToken(token); 
    
    res.locals = { id, username, email };

    next();
  } catch (error) {
    sendErrorResponse(res, error);
  }
};

module.exports = { validateUser };